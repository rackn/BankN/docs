.. Copyright (c) 2022 Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: Digital Rebar Provision; BankN

.. _rs_bankn:

BankN
~~~~~

BankN is a fake financial institution / bank that is using RackN Digital Rebar Provision to manage
its infrastructure.

The primary use of this entity is displaying how-to operate infrastructure with hands-on examples and
starting points.

.. toctree::

  bankn_demo_setup
  bankn_team
  bankn_environment


BankN Demo Actions
------------------

* Blueprint/Pipeline: Build

BankN Operation Actions
-----------------------

* Blueprint: Create DevTest environment
* Blueprint: Promote content through tree.
* Blueprint: start here



Repos:

* BankN Docs ???
* BankN Demo
* BankN DRP Operations
* BankN HW Profiles
* BankN Infrastructure Pipelines
* BankN Cluster Content Pack
* BankN Automation Piplines

---
product blueprints - Update local catalog - crontrigger. - on/off by global profile parameter value.
product blueprints -


product bootstrap-manager - set global profile parameter console to self?

