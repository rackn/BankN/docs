.. Copyright (c) 2022 Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: BankN; BankN Demo Setup

.. _rs_bankn_demo_setup:

BankN Demo Setup
~~~~~~~~~~~~~~~~

Look at the demo-setup_ repo for specific details on setting up the demo.

.. _demo-setup: https://gitlab.com/rackn/BankN/demo-setup

.. include:: demo-setup.rst


BankN Layout
~~~~~~~~~~~~

.. include:: dc-layout.rst
