.. Copyright (c) 2022 Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: BankN; BankN Team

.. _rs_bankn_team:

BankN Team
~~~~~~~~~~

Describe the Teams using DRP at BankN

.. _rs_bankn_team_roles:

BankN Team Roles
----------------

Talk about what the roles are in the BankN Team.

* DRP Operator/Maintainer

  * Update DRP and its baseline content
  * Back-up/Restore operations
  * Installation / HA setup
  * Catalog maintenance
  * MSM maintenance

* Infrastructure Developer

  * Makes Infrastructure Pipelines for Platform components
  * Meets requirements for the Platform builder

* Hardware maintainer for Infrastructure Pipeline

  * Handles hardware specific pieces of the Infrastructure Pipeline
  * Curates the hardware pieces of the Infrastructure Pipeline

* Infrastructure Operate

  * Deploys and the pieces of the Infrastructure in production
  * Discover infrastructure
  * Maintenance operations on Infrastructure
  * Rebuild infrastructure
  * Decommission infrastructure

* Cluster/Platform Developer

  * Makes Infrastructure Pipelines for Platform components
  * Meets requirements for the Platform builder

* Cluster/Platform Operator

  * Deploys the platform on pieces from the Infrastructure team
  * Build platform/cluster
  * Maintenance operations on Cluster
  * Rebuild Cluster
  * Decommission cluster

* Cluster/Platform Consumer

  * Uses the cluster/platform as a service.
  * Same pattern can continue for continuing break down.

* DRP Control Developer/Operator

  * Builds CI/CD pipelines for intergation of the following teams and helps with deployment of resources.
  * Makes pipelines that trigger upon actions of the others


Coming:

* Network Developer
* Network Operator
* Network Consumer
