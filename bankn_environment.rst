.. Copyright (c) 2022 Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license
.. index::
  pair: BankN; BankN Environment

.. _rs_bankn_environment:

BankN Environment
~~~~~~~~~~~~~~~~~

This describes the environment of BankN.  Not necessarily the demo environment.

.. _rs_bankn_drp_endpoint_layout:

BankN DRP Endpoint Layout
-------------------------

.. graphviz::

    digraph drp_layout {
        bgcolor=cyan;

       subgraph cluster_0 {
         fillcolor=lightgreen style=filled;

         mgr_test [fillcolor=lightpink style=filled label="Manager Test"];
         dev1_test [fillcolor=lightblue style=filled label="Dev1 Test"];
         dev2_test [fillcolor=lightblue style=filled label="Dev2 Test"];
         devn_test [fillcolor=lightblue style=filled label="DevN Test"];
         scale_test [fillcolor=lightblue style=filled label="Scale Test Env"];

         mgr_test -> dev1_test;
         mgr_test -> dev2_test;
         mgr_test -> devn_test;
         mgr_test -> scale_test;
         label="Test";
       }

       subgraph cluster_1 {
         label = "Prod";
         fillcolor=lightyellow style=filled;

         mgr_prod [fillcolor=lightpink style=filled label="Manager Prod"];
         us_prod [fillcolor=yellow style=filled label="US Prod"];
         amea_prod [fillcolor=yellow style=filled label="AMEA Prod"];
         apac_prod [fillcolor=yellow style=filled label="APAC Prod"];

         china_prod [fillcolor=lightblue style=filled label="China Prod"];
         japan_prod [fillcolor=lightblue style=filled label="Japan Prod"];
         aus_prod [fillcolor=lightblue style=filled label="Austraila Prod"];
         us_east_prod [fillcolor=lightblue style=filled label="US East Prod"];
         us_west_prod [fillcolor=lightblue style=filled label="US West Prod"];
         us_mid_prod [fillcolor=lightblue style=filled label="US Midwest Prod"];
         bel_prod [fillcolor=lightblue style=filled label="Belgium Prod"];
         uk_prod [fillcolor=lightblue style=filled label="UK Prod"];
         italy_prod [fillcolor=lightblue style=filled label="Italy Prod"];

         mgr_prod -> us_prod;
         mgr_prod -> amea_prod;
         mgr_prod -> apac_prod;
         apac_prod -> china_prod;
         apac_prod -> japan_prod;
         apac_prod -> aus_prod;
         us_prod -> us_east_prod;
         us_prod -> us_west_prod;
         us_prod -> us_mid_prod;
         amea_prod -> bel_prod;
         amea_prod -> uk_prod;
         amea_prod -> italy_prod;
       }

       subgraph {
         rank = same; mgr_test; mgr_prod;
       }

       mgr_test -> mgr_prod [label="Airgapped"];
    }
